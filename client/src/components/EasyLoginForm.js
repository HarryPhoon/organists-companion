import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import organistAvatar from '../static/img/pipe-organs-icon.jpg'

const styles = theme => ({
  base: {
    [theme.breakpoints.down('xs')]: {
      minWidth: '20em'
    },
    [theme.breakpoints.up('sm')]: {
      minWidth: "25em",
    },
  },
  userCard: {
    margin: ".5em",
    width: "90%"
  },
  loginCard: {
    maxWidth: "25em"
  },
  cardContent: {
    display: "flex",
    justifyContent: "space-between",
    placeItems: "center"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: "100%",
  },
  avatar: {
    width: 60,
    height: 60
  },
  user: {
    fontSize: '1.5em'
  },
  button: {
    width: "100%"
  }
})

class EasyLoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [
        {
          name: "Varhaník",
          group: "general"
        },
      ]
    }
  }

  renderLoginCards() {
    const { classes, onClick } = this.props;

    return(
      <Grid className={classes.base} container justifyContent='center' alignItems='center'>
        {this.state.users.map((user) => (
          <Grid item key={user.name} xs={12}>
            <Card elevation={2}>
              <CardActionArea id={user.group} onClick={onClick}>
                <CardContent className={classes.cardContent}>
                  <Avatar alt="Organist" src={organistAvatar} className={classes.avatar}/>
                  <h3 className={classes.user}>{user.name}</h3>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
    )
  }

  renderLoginForm() {
    const { classes, status, password, onClick, onChange, onSubmit } = this.props;

    return(
      <Card className={classes.loginCard}>
        <CardContent>
          <form onSubmit={onSubmit}>
            <Grid container spacing={1} justifyContent='center' alignItems='center'>
              <Grid item>
                <Typography variant="h6">Enter password</Typography>
              </Grid>
              <Grid item xs={12}>
                <TextField error={status.wrongCredentials} required id="password" label="Password" className={classes.textField} value={password} onChange={onChange} type="password" autoComplete="current-password" margin="normal" />
              </Grid>
              <Grid item xs={4}>
                <Button id="back" variant="contained" className={classes.button} color="secondary" onClick={onClick}>
                  Back
                </Button>
              </Grid>
              <Grid item xs={8}>
                <Button variant="contained" className={classes.button} type="submit" color="primary">
                Login
                </Button>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    )
  }
    
  render() {
    const { classes, status } = this.props;

    return(
      <div>
        {status.requireLogin ?
          this.renderLoginForm() :
          this.renderLoginCards()
        }
      </div>
    )
  }
}

export default withStyles(styles)(EasyLoginForm)
