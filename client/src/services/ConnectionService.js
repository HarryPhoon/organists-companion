import { useCallback, useEffect, useRef, useState } from 'react';

import { useAuth } from '././AuthService.js';

function getConnectionTarget(user) {
  if (!user || !user.accessToken)
    return null;

  let protocol = "ws";
  if (window.location.protocol == "https") {
    protocol = "wss";
  }

   // Add a case for own development case expression
  let address;
  switch (window.location.host) {
    case 'localhost:3000':
      address = "localhost:3001";
      break;
    case '192.168.0.109:3000':
      address = "192.168.0.109:3001";
      break;
    default:
      address = window.location.host;
      break;
  }

  return `${protocol}://${address}/api/ws?token=${user.accessToken}`;
}

export function useConnection(onMessage, onClose, onError) {
  const [connection, setConnection] = useState(null);
  const {user, refresh} = useAuth();
  const heartbeatRef = useRef(null);
  const [heartbeat, setHeartbeat] = useState(false);

  const checkConnection = () => {
    if (!connection)
      return;

    if (connection.readyState === connection.CLOSED)
      connect(user);
  };

  useEffect(checkConnection, [heartbeat]);

  const updateMessageHandler = () => {
    if (!connection)
      return;

    connection.addEventListener('message', onMessage);
    return () => { connection.removeEventListener('message', onMessage) };
  };

  const updateCloseHandler = () => {
    if (!connection)
      return;

    connection.addEventListener('close', onClose);
    return () => { connection.removeEventListener('close', onClose) };
  };

  const updateErrorHandler = () => {
    if (!connection)
      return;

    connection.addEventListener('error', onError);
    return () => { connection.removeEventListener('error', onError) };
  };

  useEffect(updateMessageHandler, [connection, onMessage]);
  useEffect(updateCloseHandler, [connection, onClose]);
  useEffect(updateErrorHandler, [connection, onError]);

  const close = useCallback(() => {
    if (!connection)
      return;

    const intervalId = heartbeatRef.current;
    clearInterval(intervalId);
    heartbeatRef.current = null;

    connection.close();
    setConnection(null);
  }, [connection]);

  const connect = () => {
    if (connection)
      close();

    const target = getConnectionTarget(user);

    let ws = null;
    try {
      ws = new WebSocket(target);
    } catch (err) {
      console.error(err);
      return false;
    }

    if (!heartbeatRef.current) {
      const intervalId = setInterval(() => {
        setHeartbeat(prevValue => !prevValue);
      }, 2000);
      heartbeatRef.current = intervalId;
    }

    setConnection(ws);
  };

  const sendMessage = useCallback((msg) => {
    console.debug(`Send message ${JSON.stringify(msg)}`);

    if (!connection)
      throw "Can not send a message without an open connection";

    connection.send(JSON.stringify(msg));
  }, [connection]);

  return [connect, close, sendMessage];
}
