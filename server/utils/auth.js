const buffer = require('buffer');
const crypto = require('crypto');
const database = require('../utils/database.js');
const tokens = require('./tokens');
const debug = require('debug')('auth');

const authAlgorithm = "aes-256-ctr";
const defaultPassword = "password";

const auth = {
  setup() {
    return new Promise(async (resolve, reject) => {
      let password = process.env.COMPANION_PASS;
      if (!password || password.length == 0) {
        password = defaultPassword;
      }

      salt = crypto.randomBytes(256);
      const key = crypto.scryptSync(`${password}`.normalize(), salt, 32);

      crypto.randomFill(new Uint8Array(16), async (err, iv) => {
        if (err)
          reject(err);

        const cipher = crypto.createCipheriv(authAlgorithm, key, iv);

        let encryptedPassword = cipher.update(password, "utf8", "hex");
        encryptedPassword += cipher.final("hex");

        let res = await database.updateOne("users", `password='${encryptedPassword}',salt='${salt.toString('hex')}',iv='${iv}'`, "username=?", "general");
      });
    });
  },

  login(req) {
    return new Promise(async (resolve, reject) => {
      try {
        let username;
        let password;

        if (req.body.username)
          username = req.body.username.normalize();

        if (req.body.password)
          password = req.body.password.normalize();

        let user = await database.selectOne("*", "users", "username", username);

        if (!user)
          reject("Unknown username");

        if (!user.password)
          resolve(user);

        const salt = Buffer.from(user.salt, 'hex');
        const key = crypto.scryptSync(`${password}`.normalize(), salt, 32);

        const iv = Uint8Array.from(user.iv.split(',').map(number => parseInt(number)));

        const decipher = crypto.createDecipheriv(authAlgorithm, key, iv);
        let decryptedPassword = decipher.update(user.password, "hex", "utf8");
        decryptedPassword += decipher.final("utf8");

        console.debug(`DB: ${user.password} ; INPUT: ${password} ; RES: ${decryptedPassword}`);

        if (password != decryptedPassword)
          reject("Incorrect password");

        resolve(user);
      }
      catch (err) {
        reject(err)
      }
    })
  },

  quickLogin(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try {
        const username = req.body.username;
        const refreshToken = req.body.refreshToken;

        if (!refreshToken)
          reject("No token");

        let user = await database.selectOne("*", "users", "username", username);

        if (!user)
          reject("Incorrect username");

        console.debug(`RT: ${JSON.stringify(refreshToken)}`);

        isTokenValid = await tokens.verifyRefreshToken(refreshToken, user);

        if (!isTokenValid)
          reject("Invalid token");

        if (username && isTokenValid)
          resolve(user);
        else
          reject("Unidentified error during quicklogin");
      }
      catch (err) {
        reject(err);
      }
    })
  },
};

module.exports = auth;
